import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone
def getTimestamp() {
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"))
    def sdf = new SimpleDateFormat("yyyyMMddHHmmss")
    return "v${sdf.format(new Date())}"
} 
pipeline {
    options{
      timeout(time: 2, unit: 'HOURS')
    }
    agent any
    parameters {
        choice(
         name: 'APP_NAME',
	           choices: ['iotss-web','iotss-backend'],
	           description: '选择需要更新的服务,前端文件以.zip格式上传,jar包以 .jar 格式上传,ftp目录内只保留当前需要发布的包文件,切勿保留额外的zip及jar文件可能导致错乱'
        )
        string(name:'TAG_NAME',defaultValue: getTimestamp(),description:'镜像名称:默认v+当前日期格式(yyyyMMddHHmmssUTC时间),必须满足v.* 格式才能触发发布操作')                  
    }

    environment {
        DOCKER_CREDENTIAL_ID = 'alibaba-registry'
        GERRIT_CREDENTIAL_ID='gerrit'
        GITHUB_CREDENTIAL_ID = 'gitlab'
        KUBECONFIG_CREDENTIAL_ID = 'alibaba-kubeconfig'
        REGISTRY = 'registry.cn-zhangjiakou.aliyuncs.com'
        DOCKERHUB_NAMESPACE = 'reconova'
        BUILD_TRIGGER_BY = "${currentBuild.getBuildCauses()[0].userId}"
    }

    stages {
      stage('parallel to executor build'){
        parallel {
          stage ('build and push image with tag for java') {
          when{
            expression{
              return !params.APP_NAME.matches('.*-web')
            }
          }
          agent {
            label 'maven'
          }          
            steps {
                container ('maven') {
                    echo "[++]start  build this project[++]"
                    sh 'mkdir docker && cd docker/ && curl  -o ./app.jar  -u lijian:"O2Thq~|T" -O   ftp://172.16.2.222/ruiwei/iotss/*.jar'
                    sh """
                      cd docker/ && cat >Dockerfile <<EOF
                       FROM openjdk:8-jdk-alpine
                       MAINTAINER lijian@reconova.com
                       WORKDIR /work
                       RUN ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' > /etc/timezone
                       ADD app.jar  app.jar
                       EXPOSE 8017
EOF
                    """
                    sh 'cd docker && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
          }
        stage ('build and push image with tag for nodejs') {
          when{
            expression{
              return params.APP_NAME=~/.*-web/
            }
          }
          agent {
            label 'nodejs'
          }          
            steps {
                container ('nodejs'){
                    echo "[++]start  build this project[++]"
                    sh 'mkdir docker && cd docker/ && curl  -o ./dist.zip  -u lijian:"O2Thq~|T" -O   ftp://172.16.2.222/ruiwei/iotss/*.zip && unzip dist.zip -d .'
                    sh """
                       cd docker/ && cat >Dockerfile <<EOF
                        FROM nginx:latest
                        MAINTAINER lijian@reconova.com
                        COPY dist /usr/share/nginx/html/
                        EXPOSE 80
EOF
                    """
                    sh 'cd docker/ && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
        }
        }

      }
      stage("parallel to execute deploy"){
        parallel{
          stage('deploy for java server'){
          when{
            expression{
              return params.TAG_NAME =~ /v.*/ && !params.APP_NAME.matches('.*-web')
            }
          }
          agent {
            label 'maven'
          }          
          steps {
              container ('maven') {
              echo "deploy to production"
              withCredentials([
                    kubeconfigFile(
                    credentialsId: env.KUBECONFIG_CREDENTIAL_ID,
                    variable: 'KUBECONFIG')
                    ]) {
                    sh 'kubectl  --insecure-skip-tls-verify set image deployment/${APP_NAME} ${APP_NAME}=${REGISTRY}/${DOCKERHUB_NAMESPACE}/${APP_NAME}:${TAG_NAME} -n house'
              }  
              }
          }
        }
        stage('deploy for web'){
          when{
            expression{
              return params.TAG_NAME =~ /v.*/ &&  params.APP_NAME.matches('.*-web')
            }
          }
          agent {
            label 'nodejs'
          }          
          steps {
              container ('nodejs') {
              echo "deploy to production"
              withCredentials([
                    kubeconfigFile(
                    credentialsId: env.KUBECONFIG_CREDENTIAL_ID,
                    variable: 'KUBECONFIG')
                    ]) {
                    sh 'kubectl --insecure-skip-tls-verify set image deployment/${APP_NAME} ${APP_NAME}=${REGISTRY}/${DOCKERHUB_NAMESPACE}/${APP_NAME}:${TAG_NAME} -n house'
                    }  
              }
          }
        }

        }
      }        
    }               
    post {
      always{
        echo "[++]send dingding message to ding group [++]"
        sh "curl 'https://oapi.dingtalk.com/robot/send?access_token=729eff552d1a6f201df4002f1edd4e0a2aa60b5339aae564177318b8e7387aa3' -H 'Content-Type: application/json' -d '{\"msgtype\": \"markdown\",\"markdown\": {\"title\": \"[网约房]智能门锁管理平台CI/CD构建通知\",\"text\":\"**网约房项目CI/CD 构建结束通知**\n\n  **构建结果:** <font color=#FF0000><b>$currentBuild.currentResult</b></font>\n\n --------------------------------------\n\n构建项目: $APP_NAME \n\n ----------------------------------\n\n构建用户: $BUILD_TRIGGER_BY \n\n ----------------------------------\n\n  构建序号: $BUILD_DISPLAY_NAME \n\n ----------------------------------\n\n  容器镜像Tag: $TAG_NAME(tag名称需满足匹配 v.*否则不会触发部署) \n\n -----------------------------------\n\n\"}}'"
      }    
  }
}

